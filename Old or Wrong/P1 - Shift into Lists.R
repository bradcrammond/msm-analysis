##########################################################################################
# R-script:     P1 - Shift into Lists.R
# Project:      MSM Employment
#
# Data used:    HILDA
# Data created: A series of lists in R
#
# Date:         04/11/2019
# Author:       Brad Crammond
#
# Purpose:      Move the data set into a series of lists that I can loop over
##########################################################################################

load("Trim_HILDA_wide.RData")

##########################################################################################
# List for mental health

mentalhealth = list(th_wide$mentalhealth_11, th_wide$mentalhealth_12, th_wide$mentalhealth_13,
                    th_wide$mentalhealth_14, th_wide$mentalhealth_15, th_wide$mentalhealth_16,
                    th_wide$mentalhealth_17)
names(mentalhealth) = (c(2011, 2012, 2013, 2014, 2015, 2016, 2017))

##################################################################################################
# List for disability

disability = list(th_wide$disability_11, th_wide$disability_12, th_wide$disability_13, th_wide$disability_14,
                  th_wide$disability_15, th_wide$disability_16, th_wide$disability_17)
names(disability) = (c(2011, 2012, 2013, 2014, 2015, 2016, 2017))

##################################################################################################
# List for cumulative_disability

cumulative_disability = list()

cumulative_disability[[1]] = disability[[1]]

for (i in 2:7) {cumulative_disability[[i]] = cumulative_disability[[i-1]] + disability[[i]]}
names(cumulative_disability) = (c(2011, 2012, 2013, 2014, 2015, 2016, 2017))
##################################################################################################
# List of the time invariant confounders

time_invariant = list(th_wide[c(1,2)])
names(time_invariant) = c(id, sex)

##################################################################################################
# list of the time varying confounders

age = list(th_wide[c(3:9)])
names(age) = (c(2011, 2012, 2013, 2014, 2015, 2016, 2017))

income = list(th_wide[c()])
