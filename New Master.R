##########################################################################################
# R-script:     00 - Master.R
# Project:      Modelling MH Effects of Employment Interventions
#               For Zoe Aitken
#
# Data used:    HILDA
# Data created:
#
# Date:         20/02/20
# Author:       Brad Crammond
#
# Purpose:      This is an update and now contains a neater version of all the
#               scripts required to run the complete analysis
##########################################################################################
# Set current working directory
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

## Library the required packages
source("Scripts/requirements.R")

##################################################################################################
##                                      DATA                                                    ##

# With Data Cleaning finished - this is the file to use
load("Data/Analysis HILDA cleaned.RData")

# For current analysis (dropping smoking, household type, social support)
load("Data/Analysis HILDA ready.RData")

##################################################################################################
## There is a list of superseded data files in:
"Scripts/old-datasets.R"

##################################################################################################
## Data Cleaning

source("00a - Data Cleaning.R")

save(th, file = "Data/Analysis HILDA cleaned.RData")

##################################################################################################
## Data Trim
#(drop unnecessary variables)
source("00b - Data Trimming.R")

##################################################################################################
## Cumulative Disability

source("Scripts/cumulative disability.R")


##################################################################################################
## Multiple Imputation

# Preparation
source("Scripts/MI0 - Multiple Imputation Prep.R")

# Run the Imputation
source("Scripts/MI1 - The actual imputation.R")

##################################################################################################
## The Marginal Structural Model

# Run the imputed datasets through the MSM
load("Data/HILDA postMI.RData")

source("Scripts/msm-runner.R")

# source("Scripts/MI1 - Multiple Imputation Runner.R")

##################################################################################################
## Analysis

source("O1 - Outcomes.R")








##################################################################################################
#   MSM Predictive Model with Inverse Probability Treatment Weighting
#   Using Only Complete Cases

source("Scripts/Complete Case Analysis Runner.R")

# which ends up with the dataset:
load("./Data/Complete_HILDA_With_Weights.RData")

#######################################################

# Analysis

## Equal Employment
source("Scripts/CC1 - Only Complete Outcomes Analysis.R")

 # The Plot
 source("Scripts/P1 - Equal Employment Plot.R")

