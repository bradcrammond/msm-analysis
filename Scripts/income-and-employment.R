## Income and Employment

load("Data/Analysis HILDA Ready.RData")

inc.emp = lm(income ~ employment, data = thc)

summary(inc.emp)

# unemployment is associated with a $370 per week reduction in income.

incemp2 = lmer(income ~ employment + (1|xwaveid), data = thc)

summary(incemp2)
## this gives a more modest reduction of $152 per week.

# Effect on MH of income and employment
mh = lmer(mentalhealth ~ income + employment + (1|xwaveid), data = thc)
summary(mh)
options(scipen = 999)

## Effect on income of disability and employment
dy = lmer(income ~ disability + employment + (1|xwaveid), data = thc)
summary(dy)

##Effect on MH of disability and employment
mhdy = lmer(mentalhealth ~ income + disability + employment + (1|xwaveid), data = thc)
summary(mhdy)

##Effect on MH of disability and employment with an interaction
dyxem = lmer(mentalhealth ~ disability + employment + disability*employment + (1|xwaveid), data = thc)
summary(dyxem)
