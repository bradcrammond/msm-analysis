##################################################################################################
## MSM Runner

# This script runs the multiply imputed datasets through the MSM function and obtains
# 3 sets of predictions for mental health at wave 17
 # - for equal employment
 # - for increased employment (by 10%)
 # - for increased participation (which allows for not everyone being able to work)

##################################################################################################
# Get the data
load("Data/HILDA postMI.RData")

# Load the function
source("Scripts/msm-function.R")
##################################################################################################
# Run the 50 imputed datasets through the msm-func
estimate = th_MI %>% mice::complete("all") %>% lapply(msm_func)

# Save the 50 output datasets 
save(estimate, file = "Data/MI list post interventions.RData")

##

