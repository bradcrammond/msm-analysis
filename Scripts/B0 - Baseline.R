# Baseline

rm(list = ls())

load("Data/Analysis HILDA cleaned.RData")

##################################################################################################

baseline = dplyr::filter(th, th$wave == 11)

b1 = baseline

library(arsenal)

tab1 = tableby(disability ~ mentalhealth + sex + employment + edlevel + quincome, data = b1)

A = summary(tab1)

##################################################################################################
#3
library(broom)

# centre age
x = mean(b1$age)
b1$cage = b1$age - x

#categorise income
b1$quincome = cut(b1$income, breaks=c(quantile(b1$income, probs = seq(0, 1, by = 0.20), na.rm =T)), 
                  labels=c("Lowest","Second","Third","Fourth","Highest"), include.lowest=TRUE)
# categorise alcohol
b1$catalc = cut(b1$meanalc, breaks = c(quantile(b1$meanalc, probs = c(0, 0.33, 0.66, 1), na.rm = T)),
                labels=c("Low", "Medium", "High"), include.lowest = T)

best = lm(mentalhealth ~ sex + employment + edlevel + cage + disability + quincome + catalc, data = b1)

A = tidy(best)

write.xlsx(A, file = "baseline2.xlsx")
