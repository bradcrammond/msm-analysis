## Complete Case Comparison

analysis = lm(mentalhealth_17 ~ sex + edlevel + collcob + empl_11 + empl_12 + empl_13 + empl_14 + empl_15 + empl_16,
              data = tinyhilda, weights = tinyhilda$capped_weights)

summary(analysis)

names = names(coef(analysis))
estimate = coef(summary(analysis))[,"Estimate"]
SE = coef(summary(analysis))[, "Std. Error"]
CI = confint(analysis)

results = cbind(names, estimate, SE, CI)

## get prediction
tinyhilda$pre_intervention = NA
tinyhilda$pre_intervention = predict.lm(analysis, na.action = na.omit)

##################################################################################################

## Intervention 1 - Equal Employment Between People With and Without Disabilities

#if (intervention == ee) {

ee = tinyhilda

for (i in 12:17) {
    
    E = paste("empl", i, sep = "_")
    
    D = paste("disability", i, sep = "_")
    
    C = table(ee[[E]], ee[[D]])
    
    x = C[2]/(C[2]+C[1])
    y = x * (C[3]+C[4])
    prob = y / C[4]
    
    #prob = ((C[2]*C[3])-(C[1]*C[4]))/((C[3]*(C[1]+C[2])))
    
    ## Now to replace prob% of those who have a disability and are unemployed
    
    A = sum(ee[[D]]==1 & ee[[E]]==0)
    # so disabled + unemployed  is a T
    
    B = rbinom(n = A, 1, prob = prob) # this creates a vector of 1s and 0s according to
    # probability above
    
    
    # change proportion of disabled unemployed to disabled employed
    ee$index = ee[[D]]==1 & ee[[E]]==0
    
    ee[[E]][ee$index == T] <- B
    
}  ## close the ee for loop

tinyhilda$equalemp = predict.lm(analysis, newdata = ee)


## Finally compare the two predicted levels of mental health

intervention = lm(mentalhealth_17 ~ equalemp, data = tinyhilda)

summary(intervention)

# Or can compare only for disabled people
est = tinyhilda
dent = dplyr::filter(est, est$disability_12 == 1 | est$disability_13 == 1 |
                         est$disability_14 == 1 | est$disability_15 == 1 |
                         est$disability_16 == 1 | est$disability_17 == 1) 

intervention = lm(mentalhealth_17 ~ equalemp, data = dent)

plot(tinyhilda$pre_intervention, tinyhilda$equalemp)
