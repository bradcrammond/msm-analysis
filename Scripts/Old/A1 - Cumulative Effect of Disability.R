##########################################################################################
# R-script:     A1 - Cumulative Effect of Disability.R
# Project:      MSM Disability Employment Mental Health
#
# Data used:    HILDA
# Data created:
#
# Date:         31/10/2019
# Author:       Brad Crammond
#
# Purpose:      To see if there is a cumulative effect of disability on mental health
##########################################################################################

options(scipen = 999)

load(".Data/trim_HILDA_clean.RData")

##########################################################################################

# disability needs to be numeric to get the row sums later
#th$disability = as.integer(th$disability) - 1   # as.integer defaults to 1 and 2

##################################################################################################
##
#                 WIDE FORMAT

th_wide <- th %>% pivot_wider(
  id_cols = c(xwaveid, sex),
  names_from = wave,
  values_from = c(age, year, wklyeqincome_capped, employment, mentalhealth, disability, edlevel)
)

anyDuplicated(th_wide$xwaveid)  # 0

#missing data for years is a bit silly

th_wide$year_11 = 2011
th_wide$year_12 = 2012
th_wide$year_13 = 2013
th_wide$year_14 = 2014
th_wide$year_15 = 2015
th_wide$year_16 = 2016
th_wide$year_17 = 2017

save(th_wide, file = "Data/Trim_HILDA_wide.Rdata")

# th_wide$dupes = duplicated(th_wide$xwaveid)

##################################################################################################


# Next is an indicator of cumulative disability

# I am going to have to treat missing values as zeros

# Be careful running this - disability is across columns 38-44 but if anything
# changes before this point, the wrong variables will get changed
for (i in 38:44) {

  th_wide[[i]][is.na(th_wide[[i]])] = 0

  table(th_wide[[i]], exclude = "no")

}

# Create a cumulative variable the nasty way
th_wide$cum_disab = th_wide$disability_11 + th_wide$disability_12 + th_wide$disability_13 +
  th_wide$disability_14 + th_wide$disability_15 + th_wide$disability_16

sum(is.na(th_wide$cum_disab))

# And examine as a factor variable as well
th_wide$factor_cum_disab = as.factor(th_wide$cum_disab)

# Then perform the regression
cumulative_effect = lm(mentalhealth_17 ~ factor_cum_disab + sex, data = th_wide)

summary(cumulative_effect)
SE = coef(summary(cumulative_effect))[, "Std. Error"]
view(SE)

CI = confint(cumulative_effect)
view(CI)

## It is worth considering if the cumulative effect drops off over time
th_wide$disab1112 = th_wide$disability_11 + th_wide$disability_12
th_wide$disab1314 = th_wide$disability_13 + th_wide$disability_14
th_wide$disab1516 = th_wide$disability_15 + th_wide$disability_16

cumulative_drop = lm(mentalhealth_17 ~ disab1112 + sex, data = th_wide)
summary(cumulative_drop)

cumulative_1314 = lm(mentalhealth_17 ~ disab1314 + sex, data = th_wide)
print(cumulative_1314)

cumulative_1516 = lm(mentalhealth_17 ~ disab1516 + sex, data = th_wide)
print(cumulative_1516)

##################################################################################################
## The Set of Cumulative Variables

th_wide$cumulative_12 = th_wide$disability_11 + th_wide$disability_12
th_wide$cumulative_13 = th_wide$disability_11 + th_wide$disability_12 + th_wide$disability_13

mentalhealth = list(th_wide$mentalhealth_11, th_wide$mentalhealth_12, th_wide$mentalhealth_13,
                    th_wide$mentalhealth_14, th_wide$mentalhealth_15, th_wide$mentalhealth_16,
                    th_wide$mentalhealth_17)
names(mentalhealth) = c(2011, 2012, 2013, 2014, 2015, 2016, 2017)

disability = list(th_wide$disability_11, th_wide$disability_12, th_wide$disability_13, th_wide$disability_14,
                  th_wide$disability_15, th_wide$disability_16, th_wide$disability_17)


cumulative_disability = list()

cumulative_disability[[1]] = disability[[1]]

for (i in 2:7) {cumulative_disability[[i]] = cumulative_disability[[i-1]] + disability[[i]]}


wave = list()

wave[[1]] = lm(mentalhealth[[2]] ~ disability[[1]] + disability[[2]] + cumulative_disability[[2]])

summary(wave[[1]])



for (i in 39:44) {

    "name" = paste("cumulative_", i-27, sep = "")



    assign(th_wide[c(name)], rowSums(th_wide[c(38:i)]))


}

th_wide = th_wide[c(1:46)]


apply(df[df$sex=="M",c("age","height","weight")], 2,
      function(x){c("n"=length(x[!is.na(x)]), "mean"=mean(x, na.rm=T), "sd"=sd(x, na.rm=T))})


apply(th_wide[c(39:44)], 2, function(x){rowsum(th_wide[c(38:x)])})
