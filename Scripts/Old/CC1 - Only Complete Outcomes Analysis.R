##################################################################################################
## The Tiniest Analysis

##################################################################################################
# At this point, just to get it working, I am going to drop everyone without a final result

# ch_wide$comp = is.na(ch_wide$capped_weights)
# table(ch_wide$comp)
#
# tinyhilda = subset(ch_wide, comp == FALSE)
# View(tinyhilda)
#
# save(tinyhilda, file = "./Data/tiny-complete-HILDA.RData")

##################################################################################################
## TINY Analysis
load("./Data/tiny-complete-HILDA.RData")

analysis = lm(mentalhealth_17 ~ sex + edlevel + collcob + age_11 +
                empl_11 + empl_12 + empl_13 + empl_14 + empl_15 + empl_16,
              data = tinyhilda, weights = tinyhilda$capped_weights)

summary(analysis)

names = names(coef(analysis))
estimate = coef(summary(analysis))[,"Estimate"]
SE = coef(summary(analysis))[, "Std. Error"]
CI = confint(analysis)

results = cbind(names, estimate, SE, CI)

write.csv(results, file = "./Output/Tiny Results.csv")

# Get a set of predicted mental health values

tinyhilda$pre_intervention = predict.lm(analysis)

## Drop some of the weights variables (just to make things easier to see)
#tinyhilda = tinyhilda[c(1:39, 44, 45, 50)]

##################################################################################################
## Intervention 1: Set employment for people with disabilities to the same as those without disability

ee = tinyhilda

for (i in 11:17) {

  E = paste("empl", i, sep = "_")

  D = paste("disability", i, sep = "_")

  C = table(ee[[E]], ee[[D]])

  prob = ((C[2]*C[3])-(C[1]*C[4]))/((C[3]*(C[1]+C[2])))

  ## Now to replace prob% of those who have a disability and are unemployed

  A = ee[[D]]==0 | ee[[E]]==1 # gives a true/false vector (which works like 0/1)
  # so disabled + unemployed  is a zero
  B = rbinom(n = length(A), 1, prob = prob) # this creates a vector of 1s and 0s according to
  # probability above

  # change proportion of disabled unemployed to disabled employed
  ee[[E]][A==F] = B[A==F]

}

##################################################################################################
## Then we re-run the predictions on a new set of data

tinyhilda$equalemp = predict.lm(analysis, newdata = ee)

save(tinyhilda, file = "Data/tiny_HILDA_ee_intervention.RData")

## And the comparison is:
#tinyhilda$pre_intervention
#tinyhilda$equalemp

summary(tinyhilda$pre_intervention)
summary(tinyhilda$equalemp)

