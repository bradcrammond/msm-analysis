## Library all the things
library(readr)         # read csv
library(tidyverse)     # everything
library(ggthemes)      # pretty graphs
library(extrafont)     # fonts for pretty graphs
library(anchors)       # using it for replace.value in data cleaning
library(mice)          # for multiple imputation
library(data.table)    # to work with lags/leads
library(medflex)       # mediation analysis
library(mediation)     # more mediation analysis
library(lme4)
library(purrr)
library(gee)           # GEEs
library(reader)        # for finding files and searching directories
library(haven)         # reading stata files
library(stringi)
library(textshape)
library(scales)       # squishing together top and bottom of variables
library(xlsx)         # write to excel
library(arsenal)      # tableby for descriptive tables
library(broom)        # tidy up lm model outputs for writing to excel
