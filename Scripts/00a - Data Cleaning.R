##################################################################################################

## 00a - Data Cleaning
## Because it never ends

## Starting Again
## Clean dataset with full set for analysis
tdf = read_dta(file = "Data/Analysis HILDA.dta")

save(tdf, file = "Analysis HILDA.RData")

load("Data/Analysis HILDA.dta")

##################################################################################################
### ID

## stored as a factor
#th$id = as.character(th$xwaveid) ## first convert to character
th$xwaveid = as.integer(th$xwaveid)        ## now can change to numeric

##################################################################################################
### Gender

th$sex = factor(th$sex, levels = c("1", "2"), labels = c("Male", "Female"))
table(th$sex)

##################################################################################################
### Age

# Age is listed as a character and when converted, there are suddenly 2900 missing values
# These are all the people listed as under 1

## Though most of that is pointless since I need to restrict to people over 15

th = filter(th, th$age > 14)

th$age = as.integer(th$age)
summary(th$age)

## Get the missing values and figure out why they are coming up as missing
# th$age2 = as.integer(th$age)
# age = th[c("age", "age2")]
# sum(is.na(th$age2))
#
# age2 = filter(age, is.na(age2))
#
# ## change them all to zero
# th$age2[is.na(th$age2)] = 0
#
# ## and go back to age
# th$age = th$age2

##################################################################################################
### Year of death

# I need to drop everyone who is dead
th$dead = !is.na(th$yodeath)
table(th$dead)

th = dplyr::filter(th, th$dead == F)

th = th[-c(5, 6)]
##################################################################################################
### Age at death
th = replace.value(th, c("aadeath"), from = "[-1] Not asked", to = as.double(NA))
th = replace.value(th, c("aadeath"), from = "-1", to = as.double(NA))
table(th$aadeath)

th$aadeath = as.numeric(th$aadeath)

##################################################################################################
### remoteness

th = replace.value(th, c("remote"), from = "[-7] Not able to be determined", to = as.double(NA))
th = replace.value(th, c("remote"), from = "-7", to = as.double(NA))


table(th$remote)

th$remote = factor(th$remote, levels = 2:6, labels = c("Major City", "Inner Regional", "Outer Regional",
                                                       "Remote", "Very Remote"))

##################################################################################################
### Employment

th$employment <- as.numeric(th$employment)
table(th$employment)
# 0 = employed
# 1 = unemployed
# 2 = NILF

## This is to combine unemployed with NILF
th$employment[th$employment == 2] <- 1
table(th$employment)

th$employment = factor(th$employment, levels = c("0", "1"), labels = c("Employed", "Unemployed"))

##################################################################################################
### Household Type

## This is a mess - with 26 different household types. I can collapse it down to 6 I think
table(th$hhtype)
th$hhtype = as.numeric(th$hhtype)
th$household = th$hhtype

th$household[th$hhtype == 24] <- 1  #lone person
th$household = replace(th$household, th$household >1.5 & th$household <12.5, 3)  # couples with people
th$household = replace(th$household, th$household > 12.5 & th$household < 21.5, 4) # single with people
th$household = replace(th$household, th$household >21.5 & th$household < 26.5, 5) # Other
th$household[th$hhtype == 1] <- 2   # couple no other people

table(th$household)

th$household = factor(th$household, levels = c("1", "2", "3", "4", "5"), labels = c("Lone", "Couple", "Couple with kids",
                                                                                    "Lone with kids", "Other"))


##################################################################################################
### Country of Birth

table(th$ancob)
th = replace.value(th, c("ancob"), from = "-10", to = as.double(NA))
th = replace.value(th, c("ancob"), from = "-4", to = as.double(NA))

## Hmm - this import has dropped all value labels
 # though they are saved as haven labels - I can probably extract them for use
th$ancob = as.factor(th$ancob)

# I kind of want to drop the codes off the front of these
# stringi does the job very nicely indeed:

th$cob = stri_sub(th$ancob, 7)

## somehow some things got left of and haven't been done consistently
th = replace.value(th, c("cob"), from = "Non-responding person", to = as.double(NA))
th = replace.value(th, c("cob"), from = "efused/Not stated", to = as.double(NA))

table(th$cob)
table(th$ancob)

## COB also poses a problem when I convert to wide format
 # There are 1200 missing values
 # I need to fill out any missing values if a person provides the answer in another wave

source("Q1 - Country of Birth.R")

#save(th, file = ".Data/trim_HILDA_clean.RData")

## I also need a truncated COB

table(th$ancob)

## Convert to character so I can extract strings
th$cob = as.numeric(th$ancob)
th$collcob = th$cob

th$collcob[th$cob == 1101] <- 1
th$collcob[th$cob == 1201 | th$cob == 2100 | th$cob == 2201 | th$cob == 8102 | th$cob == 8104] <- 2
th$collcob[th$collcob > 900] <- 3

th$collcob = as.factor(th$collcob)
table(th$collcob)

## This was all when the labels were still in the data

# ## extract the country code from the string
# th$truncob <- stri_sub(th$cob, from = 2, to = 5)
# ## and tidy up the one 3-digit code
# th = replace.value(th, c("truncob"), from = "913]", to = "913")
#
# ## convert to numeric (because the character matching is messy)
# th$truncob = as.numeric(th$truncob)
# ## create the new collapsed country of birth variable
# th$collcob = th$truncob
#
# ## recode the collapsed cob to Australia = 1, Other English Speaking = 2, and everyone else = 3
# th$collcob[th$truncob == 1101] <- 1
# th$collcob[th$truncob == 1201 | th$truncob == 2100 | th$truncob == 2201 | th$truncob == 8102 | th$truncob == 8104] <- 2
# th$collcob[th$collcob > 900] <- 3
#
# table(th$collcob)
## And now we have a three-level COB variable

th$collcob = factor(th$collcob, levels = c("1", "2", "3"), labels = c("Australia", "Other English Speaking", "Non-English Speaking"))

# drop the interim variables
th2 = th[-c(22)]
th = th2

save(th, file = "Data/Analysis HILDA cleaned.RData")

##################################################################################################
# Relationship

table(th$relationship)
th$relationship = factor(th$relationship, levels = c("0", "1"), labels = c("in relationship", "not in relationship"))## fine

##################################################################################################
### Social Support

table(th$socialsupportcat3levels)
th$socialsupport = factor(th$socialsupportcat3levels, levels = c("1", "2", "3"), labels = c("Low", "Medium", "High"))
table(th$socialsupport)
##################################################################################################
### smoking

table(th$smoke)

#collapse ex and ever smokers together
th$smoke[th$smoke == 2] <- 1

th$smoke = factor(th$smoke, levels = c("0", "1"), labels = c("Never", "Ever"))

table(th$smoke)
sum(is.na(th$smoke))

save(th, file = "Data/Analysis HILDA cleaned.RData")

##################################################################################################
### alcohol

table(th$alcohol)
th$alcohol = factor(th$alcohol, levels = c("0", "1", "2"), labels = c("No", "Sometimes", "Often"))

## I want mean alcohol consumption over the course of the study
th$numalc = as.numeric(th$alcohol)
table(th$numalc)

## Get the mean by ID
meanalc = as.data.frame(with(th, tapply(numalc, xwaveid, mean, na.rm = T)))
library(data.table)
## then tidy up by moving rownames into xwaveid, and renaming the weird column created by 'with'
setDT(meanalc, keep.rownames = "xwaveid")

meanalc$meanalc = meanalc$`with(th, tapply(numalc, xwaveid, mean, na.rm = T))`
meanalc = meanalc[,-c(2)]

meanalc$xwaveid = as.numeric(meanalc$xwaveid)

thm = left_join(th, meanalc, by = "xwaveid")
th = thm



##################################################################################################
### Education Level

table(th$edlevel)

## want to set this to highest level achieved

source("Q2 - Highest Education Level.R")

th$edlevel = factor(th$edlevel, levels = c("0", "1", "2"), labels = c("Tertiary", "Year 12", "DNF Y12"))

save(th, file = "Data/Analysis HILDA cleaned.RData")

##################################################################################################
### Mental Health

th$mentalhealth = th$std_MENT
summary(th$mentalhealth)

##################################################################################################
### disability

table(th$disab_anytype)
th$disability = factor(th$disab_anytype, levels = c("0", "1"), labels = c("No Disability", "Physical Disability"))

##################################################################################################
## Income

th$income = th$wklyeqincome_capped

##################################################################################################
### drop some things
# drop ancob, eqincome, disab_anytype and std_MENT

th = th[-c(11, 13, 15, 16)]

# drop hhtype and supportcat3levels
th = th[-c(9, 14)]

##################################################################################################
### Save all the hard work

save(th, file = "Data/Analysis HILDA cleaned.RData")
